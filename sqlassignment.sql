--1.The members details arranged in decreasing order of Date of Birth
SELECT * FROM `members` ORDER BY date_of_birth DESC; 

--2.The members details sorting using two columns(gender,date_of_birth); the first one is sorted in ascending order by default while the second column is sorted in descending order
SELECT * FROM `members` ORDER BY gender ASC,date_of_birth DESC

--3.The members details get the unique values for genders
SELECT * FROM `members` WHERE gender = (SELECT DISTINCT gender FROM members GROUP by gender HAVING COUNT(gender)=1);

--4.The movies detials get a list of movie category_id  and corresponding years in which they were released 
SELECT title,catogory_id,year_realesd FROM `movies`

--5.The members table - get total number of males and females
SELECT COUNT( gender),gender FROM members GROUP by gender

--6.The movies table - get all the movies that have the word "code" as part of the title, we would use the percentage wildcard to perform a pattern match on both sides of the word "code".
SELECT * FROM movies WHERE title LIKE '%code%'

--7.The movies table - get all the movies that were released in the year "200X" (using _ underscore wildcard)
SELECT * FROM `movies` WHERE year_realesd LIKE '200_'

--8.The movies table - get movies that were not released in the year 200x
SELECT * FROM `movies` WHERE year_realesd NOT LIKE '200_'

--9.The movies table  - movie titles in upper case letters
SELECT UCASE(title) FROM `movies`

--10.Create new table "movierentals" (see movierentals.png image)
INSERT INTO `movierentals` (`reference_number`, `transaction_date`, `return_date`, `membership_number`, `movie_id`, `movie_returned`) VALUES ('11', '2012-06-20', '', '1', '1', '0');
INSERT INTO `movierentals` (`reference_number`, `transaction_date`, `return_date`, `membership_number`, `movie_id`, `movie_returned`) VALUES ('12', '2012-06-22', '2012-06-25', '1', '2', '0'), ('13', '2012-06-22', '2012-06-25', '3', '2', '0');
INSERT INTO `movierentals` (`reference_number`, `transaction_date`, `return_date`, `membership_number`, `movie_id`, `movie_returned`) VALUES ('14', '2012-06-21', '2012-06-24', '2', '2', '0');
INSERT INTO `movierentals` (`reference_number`, `transaction_date`, `return_date`, `membership_number`, `movie_id`, `movie_returned`) VALUES ('15', '2012-06-23', '', '3', '3', '0');

--11.The movierentals table - get the number of times that the movie with id 2 has been rented out
SELECT COUNT(`movie_id`)  FROM `movierentals` WHERE `movie_id` = 2;

--12.The movierentals table - omits duplicate records which have same movie_id
Delete movie_id,COUNT(*) FROM `movie_rentals` GROUP BY movie_id HAVING COUNT(*) > 1;

--13.The movie table - latest movie year released(using MAX function)
SELECT MAX(`year_realesd`) FROM `movies`

--14.Create "payments" table (see payments.png image)
INSERT INTO `payments` (`payment_id`, `membership_number`, `payment_date`, `description`, `amount_paid`, `external_reference_number`) VALUES ('1', '1', '2012-07-23', 'movie rental payment', '2500', '11'), ('2', '1', '2012-07-25', 'movie rental payment', '2000', '12'), ('3', '3', '2012-07-30', 'movie rental payment', '6000', '');

--15.The payments table - find the average amount paid
SELECT AVG(`amount_paid`)  FROM `payments`;

--16.Add a new field to the members table
ALTER TABLE `members` ADD `enroll no` INT(20) NOT NULL AFTER `email`;

--17.Delete a database from MySQL server (DROP command is used )
DROP DATABASE sessiondemo;

--18.Delete an object (like Table , Column)from a database.(DROP command is used )
ALTER TABLE members DROP COLUMN enroll_no;

--19.DROP a table from Database
DROP TABLE members;

--20.Rename table `movierentals` TO `movie_rentals`;
RENAME TABLE movierentals  TO  movie_rentals;

--21.The member table  - changes the width of "fullname" field from 250 to 50
ALTER TABLE `members`MODIFY `full_name` char(50) NOT NULL

--22.The member table  - Getting a list of ten (10) members only from the database
SELECT * FROM members LIMIT 10

--23.The member table - gets data starting the second row and limits the results to 2
SELECT * FROM `members` LIMIT 1, 2


